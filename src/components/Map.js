import React from "react";
import { Box, Flex } from "@chakra-ui/react";

const Map = () => {
  return (
    <Flex mt="10">
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3607.973263494563!2d55.341495074444275!3d25.271484828689537!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f5dcbc5bbb9bf%3A0x698b8e46c4141c14!2sAhmed%20Khouri%20Office%20Furniture%20Est!5e0!3m2!1sen!2sin!4v1705315795114!5m2!1sen!2sin"
        width="100%"
        height="400"
        allowfullscreen=""
        loading="lazy"
        referrerpolicy="no-referrer-when-downgrade"
        iwd="0"
      ></iframe>
    </Flex>
  );
};

export default Map;
