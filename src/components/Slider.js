import React, { useState, useEffect } from "react";
import { Image } from "@chakra-ui/react";
import { clients } from "../data/companyInfo";
import Marquee from "react-fast-marquee";

const Slider = () => {
  return (
    <Marquee speed="80">
      {clients.map((client, index) => (
        <Image
          h={[30, 50, 70]}
          mx="10"
          objectFit="contain"
          key={index}
          src={client}
          alt={`Logo ${index + 1}`}
        />
      ))}
    </Marquee>
  );
};

export default Slider;
